mongoose = require('mongoose')
util = require('../commons/util')

UserSchema = mongoose.Schema(
  name: String
  email: String
  password: String
  slug: String
  lastConnection:
    type: Date
    default: Date.now)
  #more stuff like ELO or shit
UserSchema.methods = authenticate: (plainText) ->
  util.encrypt(plainText) == @password

module.exports = User = mongoose.model('User', UserSchema)
