###
  This is the Game model:
    Essential attributes->
    A user list that is present in the game at its realization
    Who is white/Black
    pgn: The whole move sequence registered so far, chess game notation (?)
    result: the result of the game.

  TODO:
  .Add MLG flag (think of scalability)
###

mongoose = require('mongoose')
Schema = mongoose.Schema
GameSchema = mongoose.Schema(
  user:
    type: Schema.ObjectId
    ref: 'User'
  white: String
  black: String
  pgn: String
  result: String)

module.exports = Game = mongoose.model('Game', GameSchema)
