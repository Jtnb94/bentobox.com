exports = module.exports = function(mongoose) {
    Schema = mongoose.Schema;

    var MsgSchema = new Schema({
        body: Object,
        send:String,
        receive:String,
        type:String,        
        createdAt: {
            type: Date,
            default: Date.now
        }
    });
    module.exports = mongoose.model('mensajes', MsgSchema);
}