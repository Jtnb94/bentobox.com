###
  This is the puzzle model, every puzzle has to follow this structure to be inserted in the database.
  Attributes:
    content: Current gamne state before solving the puzzle
    solution: Sequence of moves required to solve this puzzle, in chess game notation (?)
    coomnet:Comment to help users solve the puzzle (hints number of moves until solution)
###

mongoose = require('mongoose')
PuzzleSchema = mongoose.Schema(
  content: String
  solution: String
  comment: String)

module.exports = Puzzle = mongoose.model('Puzzle', PuzzleSchema)
