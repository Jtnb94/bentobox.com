exports = module.exports = function(mongoose) {
    Schema = mongoose.Schema;
    var PuzzleSchema = new Schema({
        type:String,
        createdBy:String,
        fenInit:String,
        fenFinish:String,
        numMoves:String,
        createdAt: {
            type: Date,
            default: Date.now
        },
        corrects: {
            type: Number,
            default: 0
        },
        intents: {
            type: Number,
            default: 0
        }        
    });
    module.exports = mongoose.model('puzzles', PuzzleSchema);
}