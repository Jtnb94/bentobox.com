###
  Passport authentication:
    Module where authentication is configured (facebook, local, etc).
###

mongoose = require('mongoose')
LocalStrategy = require('passport-local').Strategy
User = mongoose.model('User')

module.exports = (app, passport) ->
# serialize sessions
  passport.serializeUser (user, done) ->
    done null, user.id
    return
  passport.deserializeUser (id, done) ->
    User.findOne { _id: id }, (err, user) ->
      done err, user
      return
    return
  # use local strategy: Finds user that matches user//pass input in mongoDD
  passport.use new LocalStrategy({
    usernameField: 'email'
    passwordField: 'password'
  }, (email, password, done) ->
    User.findOne { email: email }, (err, user) ->
      if err
        return done(err)
      if !user
        return done(null, false, message: 'This email is not registered')
      if !user.authenticate(password)
        return done(null, false, message: 'Invalid login or password')
      done null, user
    return
  )
  return
