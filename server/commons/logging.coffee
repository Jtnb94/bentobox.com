winston       = require 'winston'
{isProduction}  = require '../../server_config'

#Loggin Setup: Basicly if its not a produciton environment it pipes logger output into console instead of file.
module.exports.setup = ->
  winston.remove(winston.transports.Console)
  if not isProduction
    winston.add(winston.transports.Console,
      colorize: true,
      timestamp: true
    )
