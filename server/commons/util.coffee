###
  Random utils module xD:
    Nice utils that we may use transparantly across the application.
###

crypto = require('crypto')
module.exports =
  #encryption using crypto lib.
  encrypt: (plainText) ->
    crypto.createHash('md5').update(plainText).digest 'hex'

  #generates a random string with "param defined" length.
  randomString: (length) ->
    chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz'
    string = ''
    i = 0
    while i < length
      randomNumber = Math.floor(Math.random() * chars.length)
      string += chars.substring(randomNumber, randomNumber + 1)
      i++
    string

  #Chooses a random color
  randomSide: () ->
    sideArray = [
      'white'
      'black'
    ]
    sideArray[Math.floor(Math.random() * sideArray.length)]
