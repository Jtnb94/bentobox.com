###
  Database common module:
    Initializes and opens the connection with mongoDB, has a couple more utils for database access.
###

config = require '../../server_config'
winston = require 'winston'
mongoose = require 'mongoose'
Grid = require 'gridfs-stream'
errors = require '../commons/errors'
_ = require 'lodash'

module.exports =
  #Utility: Checks id structure for validation purposes.
  isID: (id) -> _.isString(id) and id.length is 24 and id.match(/[a-f0-9]/gi)?.length is 24

  #Opens a connection with mongo DataBase, keeps it alive until server is terminated.
  connect: () ->
    address = module.exports.generateMongoConnectionString()
    winston.info "Connecting to Mongo with connection string #{address}, readpref: #{config.mongo.readpref}"

    mongoose.connect address
    mongoose.connection.once 'open', -> Grid.gfs = Grid(mongoose.connection.db, mongoose.mongo)

  #Generates the connection string based on the config file properties
  generateMongoConnectionString: ->
    dbName = config.mongo.db
    address = config.mongo.host + ':' + config.mongo.port
    if config.mongo.username and config.mongo.password
      address = config.mongo.username + ':' + config.mongo.password + '@' + address
    address = "mongodb://#{address}/#{dbName}"

    return address
