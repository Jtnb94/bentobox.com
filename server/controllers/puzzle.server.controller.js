var mongoose = require('mongoose'); 
var async = require('async');
var Puzzle = mongoose.model('puzzles');
var userController= require('./user.server.controller');

exports.add = (req, res) => {
	if (req.body.mode === 'add') {
		var newPuzzle= new Puzzle(req.body);
		newPuzzle.save(function(err, board) {
			if(err){
				return res.send(500, err.message);       
			}
			res.status(200).send(board)
		});
	} else {
		Puzzle.update(
			{_id: req.body.mode},
			{$set: req.body
			}, function(err) {
				if (err)
					return res.send(500, err.message);
				else
					res.status(200).send(req.body)
			});
	}
}
exports.get = (req, res) => {
	Puzzle.find(req.body)
	.sort({created: 1})
	.exec(function(err, puzzles) {
		res.status(200).send(puzzles)
	})
}

exports.resolve = (req, res) => {
	Puzzle.update(
		{_id: req.body._id},
		{$inc: {
			corrects: req.body.resolve,
			intents: 1
		}
	}, function(err) {
		if (err)
			console.log(err)
		res.status(200).send('ok')		
	});
}

exports.count = (req, res) => {
	async.parallel({
		MateIn1: function(callback) {
			Puzzle.count({
				type: 'MateIn1',
			}, function(err, count) {
				callback(null, count);
			})
		},
		MateIn2: function(callback) {
			Puzzle.count({
				type: 'MateIn2',
			}, function(err, count) {
				callback(null, count);
			})
		},
		MateIn3: function(callback) {
			Puzzle.count({
				type: 'MateIn3',
			}, function(err, count) {
				callback(null, count);
			})
		},		
		FindFork: function(callback) {
			Puzzle.count({
				type: 'FindFork',
			}, function(err, count) {
				callback(null, count);
			})
		},
		TakePiece: function(callback) {
			Puzzle.count({
				type: 'TakePiece',
			}, function(err, count) {
				callback(null, count);
			})
		}		
	},
	function(e,data){
		if (e){
			return res.send(500, e); 
		}
		return res.status(200).send(data)
	});
}


exports.deletePuzzle = (req, res) => {
	// validar se o user é o owner do puzzle
	userController.getToken(req,function(err, token){
		if (err){
			return res.status(403).send(err)
		}
		var idPuzzle = req.params.parms
		Puzzle.remove({_id: idPuzzle}, function(error) {
			if (error) {
				return res.send(500, error.message);
			} else {
				res.status(200).send('ok')
			}
		});			
	})
}

var InitPuzzles = () => {
	setTimeout(function() {
		Puzzle.count({}, function(err, count) {
			if (count === 0) {
				for (var i in puzzlesInitDefault) {
					var newPuzzle = new Puzzle(puzzlesInitDefault[i]);
					newPuzzle.save(function(err, puzzle) {
						// console.log(puzzle)
					});
				}
			} else {
				// console.log('ya hay puzzles por defecto')
			}
		})
	}, 3000);
}
InitPuzzles()

//Puzzles default a iniciar, pus um exemplo que saquei dum site mas depois é melhor apagar isto e por em db apenas
var puzzlesInitDefault = [
{
	"type" : "MateIn1",
	"createby" : "bento",
	"feninit" : "4Q2N/4q2k/2b4r/2pPBP2/2P3p1/5n2/1K4p1/7R",
	"fenfinish" : "",
	"nummoves" : "1"
}

]