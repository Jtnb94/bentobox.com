###
  Homepage:
    Has main page routes
###

express = require 'express'
passport = require 'passport'

mongoose = require 'mongoose'
User = require '../models/User'
Puzzle = require '../models/Puzzle'

router = express.Router()

router.get '/', (req, res) ->
  Puzzle.find {}, (err, puzzles) ->
    randomPuzzle = puzzles[Math.floor(Math.random() * puzzles.length)]
    logoutSuccessMessage = req.flash('logoutSuccess')
    welcomeMessage = req.flash('welcomeMessage')
    registerSuccessMessage = req.flash('registerSuccessMessage')
    res.render 'partials/index',
      title: 'Game Roulette'
      puzzle: randomPuzzle
      logoutSuccessMessage: logoutSuccessMessage
      welcomeMessage: welcomeMessage
      registerSuccessMessage: registerSuccessMessage
      user: req.user
      isHomePage: true
    return
  return

router.get '/game/:token/:side', (req, res) ->
  token = req.params.token
  side = req.params.side
  res.render 'partials/game',
    title: 'Game Roulette - Game ' + token
    user: req.user
    isPlayPage: true
    token: token
    side: side
  return
router.get '/logout', (req, res) ->
  req.logout()
  req.flash 'logoutSuccess', 'You have been successfully logged out'
  res.redirect '/'
  return
router.get '/tv', (req, res) ->
  res.render 'partials/tv',
    title: 'Game Roulette - Tv'
    user: req.user
    isTvPage: true
    opponent1: 'V. Anand'
    opponent2: 'G. Kasparov'
  return
router.get '/monitor', (req, res) ->

  mongoStatus = 'success'
  mongoIcon = 'smile'
  apiStatus = 'success'
  apiIcon = 'smile'
  esStatus = 'success'
  esIcon = 'smile'
  res.render 'partials/monitor',
    title: 'Game Roulette - Monitor'
    user: req.user
    status:
      mongo: mongoStatus
      api: apiStatus
      es: esStatus
    icon:
      mongo: mongoIcon
      api: apiIcon
      es: esIcon
  return
module.exports = router

