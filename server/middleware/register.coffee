###
  Register route:
    Here's the registration logic of the application, it just validates and inserts(after)
     into the db the new user to be created
###

express = require 'express'
mongoose = require 'mongoose'
passport = require 'passport'
util = require '../commons/util'
User = require '../models/User'
router = express.Router()

router.get '/', (req, res) ->
  errors = req.flash('error')
  error = ''
  if errors.length
    error = errors[0]
  res.render 'partials/register',
    title: 'Game Hub - Register'
    error: error
    isLoginPage: true
  return
router.post '/', (req, res, next) ->
  email = req.body.email
  name = req.body.userName
  password = req.body.password
  confirmPassword = req.body.confirmPassword
  User.findOne { email: email }, (err, user) ->
    if user != null
      req.flash 'registerStatus', false
      req.flash 'error', 'We have already an account with email: ' + email
      res.redirect '/register'
    else
# no user found
      if password == confirmPassword
        u = new User(
          name: name
          email: email
          password: util.encrypt(password))
        u.save (err) ->
          if err
            next err
          else
            console.log 'new user:' + u
            req.login u, (err) ->
              if err
                return next(err)
              req.flash 'registerStatus', true
              req.flash 'registerSuccessMessage', 'Welcome ' + u.name + '!'
              res.redirect '/'
          return
      else
        req.flash 'registerStatus', false
        req.flash 'error', 'The confirmation password does not match the password'
        res.redirect '/register'
    return
  return
module.exports = router
