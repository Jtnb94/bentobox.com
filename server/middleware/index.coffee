###
  Index file for middleware modules:
    Wire it all up here :)
###
module.exports =
  headers: require './headers'
  healthcheck: require './healthcheck'
  api: require './api'
  play: require './play'
  homepage: require './homepage'
  login: require './login'
  register: require './register'
  account: require './account'
  search: require './search'
  contact: require './contact'
