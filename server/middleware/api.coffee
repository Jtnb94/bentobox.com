###
  Api:
    Cause every modern app needs one xD.
###

express = require('express')
mongoose = require('mongoose')
router = express.Router()

### display game. ###

router.get '/game/:id', (req, res) ->
  id = req.params.id
  mongoose.model('Game').findById id, (err, game) ->
    if err
      res.status(500).end()
    if game == null
      res.status(404).end()
    else
      res.send game
    return
  return

### display user. ###

router.get '/user/:name', (req, res) ->
  name = req.params.name
  mongoose.model('User').findOne { name: name }, (err, user) ->
    if err
      res.status(500).end()
    if user == null
      res.status(404).end()
    else
      res.send
        id: user._id
        name: user.name
        email: user.email
        lastConnection: user.lastConnection
    return
  return

### api status, for monitor ###

router.get '/', (req, res) ->
  res.status(200).end()
  return
module.exports = router
