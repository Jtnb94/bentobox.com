###
  Game Routes:

###
express = require('express')
util = require('../commons/util')
router = express.Router()

router.get '/', (req, res) ->
  res.render 'partials/play',
    title: 'Game Roulette - Game'
    user: req.user
    isPlayPage: true
  return
router.post '/', (req, res) ->
  side = if req.body.side == 'random' then util.randomSide() else req.body.side
  #var opponent = req.body.opponent; NOT IMPL
  token = util.randomString(20)
  res.redirect '/game/' + token + '/' + side
  return
module.exports = router

