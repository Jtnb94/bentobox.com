###
  Search Route:
    OK bois, so here we will take advantage of elastisearch api,
    so check games in (almost) real-time with indexing and everything else
###

express = require('express')
elasticsearch = require('elasticsearch')
client = new (elasticsearch.Client)
# default to localhost:9200
router = express.Router()

router.get '/', (req, res) ->
  res.render 'partials/search',
    title: 'Game Roulette - Search'
    user: req.user
    isSearchPage: true
  return
router.post '/', (req, res) ->
  white = req.body.white
  black = req.body.black
  content = req.body.content
  result = req.body.result
  #elasticsearch defined client
  client.search(
    index: 'gameroulette'
    type: 'game'
    body: 'query': 'bool': 'should': [
      { 'match': 'white': white }
      { 'match': 'black': black }
      { 'match': 'content': content }
      { 'match': 'result': result }
    ]).then ((resp) ->
      games = resp.hits.hits
      res.set 'Content-Type', 'application/json'
      res.status 200
      res.send games: games
    ), (err) ->
      res.status 500
      console.log err

module.exports = router
