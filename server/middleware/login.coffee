###
  Login Routes
    Uses passport common module for local strategy authentication.
    Facebook login is going to be implemented in the near future
###

express = require('express')
mongoose = require('mongoose')
passport = require('passport')
User = require '../models/User'
router = express.Router()

#Triggered: when user visits /login
router.get '/', (req, res) ->
  errors = req.flash('error')
  error = ''
  if errors.length
    error = errors[0]
  res.render 'partials/login',
    title: 'Game Roulette - Login'
    error: error
    isLoginPage: true
  return

#When POST method is received, triggers this login event.
router.post '/', passport.authenticate('local',
  failureRedirect: '/login'
  failureFlash: true), (req, res) ->
  User.findOneAndUpdate { _id: req.user._id }, { lastConnection: new Date }, {}, (err, user) ->
    req.flash 'welcomeMessage', 'Welcome ' + user.name + '!'
    res.redirect '/'
    return
  return
module.exports = router
