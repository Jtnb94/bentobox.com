###
  Healthcheck:
    Checks if the User CRUD is functional
###
wrap = require 'co-express'
errors = require '../commons/errors'

module.exports = wrap (req, res) ->
  User = require '../models/User'
  user = yield User.findOne({})
  throw new errors.InternalServerError('No Users found.') unless user
  hcUser = yield User.findOne({slug: 'healthcheck'})
  if not hcUser
    hcUser = new User({
      name: 'healthcheck'
      email: 'dev.healthcheck@game-roulette.net'
      slug: 'healthcheck'
      password: '666'
    })
    yield hcUser.save()
  res.status(200).send('OK')
