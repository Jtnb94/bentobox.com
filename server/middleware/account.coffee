###
  Account Routing:
    Display info about account
    Gives option to update settings in db
###
express = require('express')
mongoose = require('mongoose')
util = require('../commons/util')
router = express.Router()
moment = require('moment')

### GET user account details. ###

router.get '/', (req, res) ->
  res.render 'partials/account',
    title: 'Game Roulette - Account'
    user: req.user
    isAccountPage: true
    lastConnection: moment(req.user.lastConnection).fromNow()
    updateStatus: req.flash('updateStatus')
    updateMessage: req.flash('updateMessage')
  return

### Update user account. ###

router.post '/', (req, res) ->
  User = require '../models/User'
  currentPassword = req.body.password
  newPassword = req.body.newPassword
  confirmNewPassword = req.body.confirmNewPassword
  hash = util.encrypt(currentPassword)
  if hash == req.user.password
    if newPassword == confirmNewPassword
      newPasswordHash = util.encrypt(newPassword)
      User.findOneAndUpdate { _id: req.user._id }, { password: newPasswordHash }, {}, (err, user) ->
        req.user = user
        req.flash 'updateStatus', true
        req.flash 'updateMessage', 'Your password has been updated successfully'
        res.redirect '/account'
        return
    else
      req.flash 'updateStatus', false
      req.flash 'updateMessage', 'The confirmation password does not match the new password'
      res.redirect '/account'
  else
    req.flash 'updateStatus', false
    req.flash 'updateMessage', 'The current password is incorrect'
    res.redirect '/account'
  return
module.exports = router
