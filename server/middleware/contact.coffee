###
  Email middleware:
    Envia mensagem de teste.
    Estrutura e envia mensagens
###
nodeMailer = require 'nodemailer'
config     = require '../../server_config'
wrap       = require 'co-express'
errors     = require '../commons/errors'

module.exports =
  sendInfoMail: wrap (req, res, next) ->
    #return res.end() unless req.user
    transporter = nodeMailer.createTransport(config.smtp);

    mailOptions =
      from: 'grContactMiddleware'
      to: 'jrodriguesonab@gmail.com'
      subject: '[GR_INFO_MAIL]'
      text: 'Game on?'
      html: '<iframe src="http://10.177.4.30:3000/"></iframe>'
    # send mail with defined transport object
    transporter.sendMail mailOptions, (error, info) ->
      if error
        return next(new errors.InternalServerError("Error sending test Email."))
      else
        res.status(200).send()

