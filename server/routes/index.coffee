###
  Middleware WireUp:
    Here is where routes are "connected" to their respective middleware module

    HOWTO WIREUP:
      First create route logic and place it in a 'middleware/' module.
      wire it up here like (app.'method'([route, mw.'module'])
###

mw = require '../middleware'

module.exports.setup = (app) ->
  app.use('/', mw.homepage); #Homepage route
  app.use('/login', mw.login); #Login route
  app.use('/register', mw.register); #Register Route
  app.use('/account', mw.account); #Account profile route
  app.use('/play', mw.play); #Game route
  app.use('/api', mw.api); # TODO: api docs
  app.use('/search', mw.search); # Elasticsearch api

  app.post('/contact/sendInfoMail', mw.contact.sendInfoMail) #Send TestEmail

  #Utility, not required for game functionality
  app.all('/headers', mw.headers)
  app.get('/healthcheck', mw.healthcheck)
