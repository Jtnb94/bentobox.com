###
  Route loader:
    base routes are defined in commons/mapping folder
    Every mapping is linked to its respective coffeescript module and will be loaded by this script
###

log = require 'winston'
routes = require('../commons/mapping').routes # as routes tão todas aqui

module.exports.setup = (app) ->
  for route in routes
    do (route) ->
      module = require('../'+route)
      module.setup app
      log.debug "route module #{route} adicionado"
